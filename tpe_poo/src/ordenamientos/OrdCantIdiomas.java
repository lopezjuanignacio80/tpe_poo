package ordenamientos;

import java.util.Comparator;

import clasesReality.ParticipanteAbstracto;

public class OrdCantIdiomas implements Comparator<ParticipanteAbstracto>{

	@Override
	public int compare(ParticipanteAbstracto o1, ParticipanteAbstracto o2) {
		// TODO Auto-generated method stub
		return o1.getCantIdiomas() - o2.getCantIdiomas();
	}

}
