package ordenamientos;

import java.util.Comparator;

import clasesReality.ParticipanteAbstracto;

public class OrdCompuesto implements Comparator<ParticipanteAbstracto>{
	private Comparator<ParticipanteAbstracto> ord1,ord2;
	

	public OrdCompuesto(Comparator<ParticipanteAbstracto> ord1, Comparator<ParticipanteAbstracto> ord2) {
		super();
		this.ord1 = ord1;
		this.ord2 = ord2;
	}

	@Override
	public int compare(ParticipanteAbstracto o1, ParticipanteAbstracto o2) {
		// TODO Auto-generated method stub
		int res= ord1.compare(o1, o2);
		if (res==0 && ord2!=null) {
			return ord2.compare(o1, o2);
		}
		return res;
	}

}
