package ordenamientos;

import java.util.Comparator;

import clasesReality.ParticipanteAbstracto;

public class OrdCantGeneros implements Comparator<ParticipanteAbstracto>{

	@Override
	public int compare(ParticipanteAbstracto o1, ParticipanteAbstracto o2) {
		// TODO Auto-generated method stub
		return o1.getCantGenerosMusicales() - o2.getCantGenerosMusicales();
	}

}
