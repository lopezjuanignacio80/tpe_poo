package ordenamientos;

import java.util.Comparator;

import clasesReality.ParticipanteAbstracto;

public class OrdEdad implements Comparator<ParticipanteAbstracto>{

	@Override
	public int compare(ParticipanteAbstracto o1, ParticipanteAbstracto o2) {
		// TODO Auto-generated method stub
		return o1.getEdad() - o2.getEdad();
	}

}
