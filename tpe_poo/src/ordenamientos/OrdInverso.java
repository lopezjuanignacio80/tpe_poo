package ordenamientos;

import java.util.Comparator;

import clasesReality.ParticipanteAbstracto;

public class OrdInverso implements Comparator<ParticipanteAbstracto>{
	private Comparator<ParticipanteAbstracto> incluido;
	
	public OrdInverso(Comparator<ParticipanteAbstracto> incluido) {
		super();
		this.incluido = incluido;
	}

	@Override
	public int compare(ParticipanteAbstracto o1, ParticipanteAbstracto o2) {
		// TODO Auto-generated method stub
		return -incluido.compare(o1, o2);
	}

}
