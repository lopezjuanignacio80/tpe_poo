package clasesReality;

import java.util.ArrayList;

import filtros.Filtro;

public class Solista extends ParticipanteAbstracto{
	private int edad;
	private ArrayList<String> generosMusicales;
	private ArrayList<String> idiomas;
	private ArrayList<String> instrumentos;
	
	public Solista(String nombre, int edad) {
		super(nombre);
		this.edad = edad;
		this.generosMusicales=new ArrayList<>();
		this.idiomas=new ArrayList<>();
		this.instrumentos=new ArrayList<>();
	}
	
	
	
	public Solista(String nombre, int edad, ArrayList<String> generosMusicales, ArrayList<String> idiomas,
			ArrayList<String> instrumentos) {
		super(nombre);
		this.edad = edad;
		this.generosMusicales = new ArrayList<>(generosMusicales);
		this.idiomas = new ArrayList<>(idiomas);
		this.instrumentos =new ArrayList<>(instrumentos);
	}



	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public void addGeneroMusical(String genero) {
		if (!generosMusicales.contains(genero))
			generosMusicales.add(genero);
	}
	public void addIdioma(String idioma) {
		if (!idiomas.contains(idioma))
			idiomas.add(idioma);
	}
	public void addInstrumento(String instrumento) {
		if (!instrumentos.contains(instrumento))
			instrumentos.add(instrumento);
	}
	
	public void removeGeneroMusical(String genero) {
		if (generosMusicales.contains(genero))
			generosMusicales.remove(genero);
	}
	public void removeIdioma(String idioma) {
		if (idiomas.contains(idioma))
			idiomas.remove(idioma);
	}
	public void removeInstrumentos(String instrumento) {
		if (instrumentos.contains(instrumento))
			instrumentos.remove(instrumento);
	}
	
	public ArrayList<String> getIdiomas(){
		return new ArrayList<String>(idiomas);
	}
	
	public ArrayList<String> getInstrumentos(){
		return new ArrayList<String>(instrumentos);
	}
	
	public ArrayList<String> getGenerosMusicales(){
		return new ArrayList<String>(generosMusicales);
	}
	
	public boolean cantaIdioma(String idioma) {
		return idiomas.contains(idioma);
	}

	public boolean prefiereGenero(String genero) {
		return generosMusicales.contains(genero);
	}
	

	public boolean tocaInstrumento(String instrumento) {
		return instrumentos.contains(instrumento);
	}

	@Override
	public ArrayList<ParticipanteAbstracto> getListadoParticipantesEspecificos(Filtro criterio) {
		ArrayList<ParticipanteAbstracto> listadoParticipantesEspecificos = new ArrayList<>();
		if (criterio.cumple(this)) {
			listadoParticipantesEspecificos.add(this.copia());
		}
		return listadoParticipantesEspecificos;
	}

	@Override
	public boolean todosTienenGenero(String generoMusical) {
		return prefiereGenero(generoMusical);
	}

	@Override
	public int getPersonasTocanInstrumento(String instrumento) {
		if (this.tocaInstrumento(instrumento))
			return 1;
		return 0;
	}

	@Override
	public ParticipanteAbstracto copia() {
		return new Solista(this.getNombre(), this.getEdad(), this.getGenerosMusicales(), this.getIdiomas(), this.getInstrumentos());	
	}
	
}
