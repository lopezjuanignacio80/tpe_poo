package clasesReality;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import filtros.Filtro;

public abstract class ParticipanteAbstracto {
	private String nombre;
	
	public ParticipanteAbstracto(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public abstract ArrayList<String> getIdiomas();
	public abstract ArrayList<String> getInstrumentos();
	public abstract ArrayList<String> getGenerosMusicales();
	public abstract int getEdad();
	public abstract boolean tocaInstrumento(String instrumento);
	public abstract boolean prefiereGenero(String genero);
	public abstract boolean cantaIdioma(String idioma);
	public abstract ArrayList<ParticipanteAbstracto> getListadoParticipantesEspecificos(Filtro criterio);
	public abstract boolean todosTienenGenero(String generoMusical);
	public abstract int getPersonasTocanInstrumento(String instrumento);
	public abstract ParticipanteAbstracto copia();

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipanteAbstracto other = (ParticipanteAbstracto) obj;
		return Objects.equals(this.getNombre(), other.getNombre());
	}	
	
	public int getCantIdiomas() {
		return this.getIdiomas().size();
	}
	public int getCantInstrumentos() {
		return this.getInstrumentos().size();
	}
	public int getCantGenerosMusicales() {
		return this.getGenerosMusicales().size();
	}
}
