package clasesReality;

import java.util.ArrayList;

import filtros.Filtro;

public class GrupoBanda extends ParticipanteAbstracto {
	private ArrayList<ParticipanteAbstracto> elementos;
	
	public GrupoBanda(String nombre) {
		super(nombre);
		elementos= new ArrayList<>();
	}
	@Override
	public ArrayList<String> getIdiomas() {
		ArrayList<String> idiomasGrupo = new ArrayList<>(); 
		for (ParticipanteAbstracto  participante: elementos) {
			ArrayList<String> idiomasParticipante = participante.getIdiomas();
			for (String  itemParticipante: idiomasParticipante) {
				if (!idiomasGrupo.contains(itemParticipante)) {
					idiomasGrupo.add(itemParticipante);
				}
			}
		}
		return idiomasGrupo;
	}

	@Override
	public ArrayList<String> getInstrumentos() {
		ArrayList<String> instrumentosGrupo = new ArrayList<>(); 
		for (ParticipanteAbstracto  participante: elementos) {
			ArrayList<String> instrumentosParticipante = participante.getInstrumentos();
			for (String  itemParticipante: instrumentosParticipante) {
				if (!instrumentosGrupo.contains(itemParticipante)) {
					instrumentosGrupo.add(itemParticipante);
				}
			}
		}
		return instrumentosGrupo;	
	}
	
	@Override
	public ArrayList<String> getGenerosMusicales() {
		//se recorren todos los generos del primer elemento, y se verifica cuales
		//se encuentran en el resto de la lista de elementos (la intersección)
		ArrayList<String> generosMusicales= new ArrayList<>();
		if (tieneElementos()) {
			ArrayList<String> generosMusicalesPrimerElemento= elementos.get(0).getGenerosMusicales();
			for (String generoMusical : generosMusicalesPrimerElemento) {
				if (this.todosTienenGenero(generoMusical)) {
					generosMusicales.add(generoMusical);
				}
			}
		}
		return generosMusicales;
	}

	public boolean todosTienenGenero(String generoMusical) {
		for (ParticipanteAbstracto participante : elementos) {
			if (!participante.todosTienenGenero(generoMusical)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean tieneElementos() {
		return (elementos.size()>0);
	}
	
	@Override
	public int getEdad() {
		// TODO Auto-generated method stub
		int edad=0;
		for (ParticipanteAbstracto participante : elementos) {
			edad+=participante.getEdad();
		}
		if (this.tieneElementos()) {
			return (int) edad/elementos.size();
		}
		return 0;
	}
	
	@Override
	public boolean tocaInstrumento(String instrumento) {
		// TODO Auto-generated method stub
		return this.getInstrumentos().contains(instrumento);
	}
	@Override
	public boolean prefiereGenero(String genero) {
		return this.getGenerosMusicales().contains(genero);
	}
	@Override
	public boolean cantaIdioma(String idioma) {
		// TODO Auto-generated method stub
		return this.getIdiomas().contains(idioma);
	}
	@Override
	public ArrayList<ParticipanteAbstracto> getListadoParticipantesEspecificos(Filtro criterio) {
		// TODO Auto-generated method stub
		ArrayList<ParticipanteAbstracto> listadoParticipantesEspecificos = new ArrayList<>();
		if (criterio.cumple(this)) {
			listadoParticipantesEspecificos.add(this.copia());
		}else {
			for (ParticipanteAbstracto participante : elementos) {
				listadoParticipantesEspecificos.addAll(participante.getListadoParticipantesEspecificos(criterio));
			}
		}
		return listadoParticipantesEspecificos;
	}
	@Override
	public int getPersonasTocanInstrumento(String instrumento) {
		int i=0;
		for (ParticipanteAbstracto p:elementos) {
			i+=p.getPersonasTocanInstrumento(instrumento);
		}
		return i;
	}
	@Override
	public ParticipanteAbstracto copia() {
		GrupoBanda copia = new GrupoBanda (this.getNombre());
		for (ParticipanteAbstracto p: elementos) {
			copia.add(p.copia());
		}
		return copia;
	}
	private void add(ParticipanteAbstracto p) {
		if(!elementos.contains(p))	
			elementos.add(p);
	}
	
}
