package clasesReality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

import filtros.Filtro;
import ordenamientos.OrdInverso;

public class JuradoCoach {
	private ArrayList<ParticipanteAbstracto> equipo;
	private String nombre;
	private String apellido;
	private Filtro restriccionIngreso;
	private Comparator<ParticipanteAbstracto> formaDeGanarActual;
	//la forma de ganar se setea cuando el jurado ingresa a un reality
	
	public JuradoCoach(String nombre, String apellido) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.equipo=new ArrayList<>();
	}
	
	public JuradoCoach(String nombre, String apellido, Filtro restriccion) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.equipo=new ArrayList<>();
		this.restriccionIngreso=restriccion;
	}
	
	public ArrayList<String> getListadoGenerosMusicales() {
		ArrayList<String> listadoEquipo = new ArrayList<>(); 
		for (ParticipanteAbstracto  participante: equipo) {
			ArrayList<String> generosParticipante = participante.getGenerosMusicales();
			for (String  itemParticipante: generosParticipante) {
				if (!listadoEquipo.contains(itemParticipante)) {
					listadoEquipo.add(itemParticipante);
				}
			}
		}
		Collections.sort(listadoEquipo);
		return listadoEquipo;	
	}
	
	public ArrayList<String> getListadoIdiomas() {
		ArrayList<String> listadoEquipo = new ArrayList<>(); 
		for (ParticipanteAbstracto  participante: equipo) {
			ArrayList<String> idiomasParticipante = participante.getIdiomas();
			for (String  itemParticipante: idiomasParticipante) {
				if (!listadoEquipo.contains(itemParticipante)) {
					listadoEquipo.add(itemParticipante);
				}
			}
		}
		return listadoEquipo;	
	}
	
	public ArrayList<String> getListadoInstrumentos() {
		ArrayList<String> listadoEquipo = new ArrayList<>(); 
		for (ParticipanteAbstracto  participante: equipo) {
			ArrayList<String> instrumentosParticipante = participante.getInstrumentos();
			for (String  itemParticipante: instrumentosParticipante) {
				if (!listadoEquipo.contains(itemParticipante)) {
					listadoEquipo.add(itemParticipante);
				}
			}
		}
		return listadoEquipo;	
	}
	public boolean tieneParticipantes() {
		return equipo.size()>0;
	}
	public double getPromedioEdadEquipo() {
		int acumEdades=0;
		for (ParticipanteAbstracto  participante: equipo) {
			acumEdades+=participante.getEdad();
		}
		if (this.tieneParticipantes()) {
			return acumEdades/equipo.size();
		}
		return 0;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public ArrayList<ParticipanteAbstracto> getListadoParticipantes (){
		return new ArrayList<>(equipo);
	}

	public ArrayList<ParticipanteAbstracto> getListadoParticipantesEspecificos(Filtro criterio){
		ArrayList<ParticipanteAbstracto> listadoParticipantesEspecificos = new ArrayList<>();
		for (ParticipanteAbstracto participante : equipo) {
			listadoParticipantesEspecificos.addAll(participante.getListadoParticipantesEspecificos(criterio));
		}
		return listadoParticipantesEspecificos;
	}
	

	public void setFormaDeGanarActual(Comparator<ParticipanteAbstracto> formaDeGanarActual) {
		this.formaDeGanarActual = formaDeGanarActual;
	}

	public ArrayList<ParticipanteAbstracto> getListadoParticipantesOrdenados(){
		//se podría pensar en llamar a este método desde la clase Reality, si es que es necesario este listado
		//para nuestra implementación, no es necesario (la consigna no aclara, es solo un servicio más)
		ArrayList<ParticipanteAbstracto> listadoParticipantesOrdenados = new ArrayList<>(equipo);
		if (this.formaDeGanarActual!=null) {
			Collections.sort(listadoParticipantesOrdenados, new OrdInverso(this.formaDeGanarActual));	
		}
		return listadoParticipantesOrdenados;
	}
	
	public void addParticipante(ParticipanteAbstracto p1) {
		if (!equipo.contains(p1)){
			if(restriccionIngreso!=null) {
				if (restriccionIngreso.cumple(p1))
					equipo.add(p1);
			}else equipo.add(p1);
		}
	}
	
	public void setRestriccionIngreso(Filtro restriccionIngreso) {
		this.restriccionIngreso = restriccionIngreso;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JuradoCoach other = (JuradoCoach) obj;
		return Objects.equals(this.getApellido(), other.getApellido()) && Objects.equals(this.getNombre(), other.getNombre());
	}		
}
