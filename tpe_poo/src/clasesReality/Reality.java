package clasesReality;
import java.util.ArrayList;
import java.util.Comparator;

public class Reality {
	private ArrayList<JuradoCoach> juradosCoachs;
	private ArrayList<TemaMusical> temasMusicales;
	private Comparator<ParticipanteAbstracto> formaDeGanar;
	
	public Reality(Comparator<ParticipanteAbstracto> formaDeGanar) {
		this.formaDeGanar = formaDeGanar;
		this.juradosCoachs= new ArrayList<>();
		this.temasMusicales= new ArrayList<>();
	}
	public void addJurado(JuradoCoach j) {
		if (!juradosCoachs.contains(j)) {
			j.setFormaDeGanarActual(formaDeGanar);
			juradosCoachs.add(j);
		}
	}
	public void addTema(TemaMusical t) {
		if (!temasMusicales.contains(t)) {
			temasMusicales.add(t);
		}
	}
	public void setFormaDeGanar(Comparator<ParticipanteAbstracto> formaDeGanar) {
		this.formaDeGanar = formaDeGanar;
		for (JuradoCoach juradoCoach : juradosCoachs) {
			juradoCoach.setFormaDeGanarActual(formaDeGanar);
		}
	}
	
	public ParticipanteAbstracto ganadorBatalla (ParticipanteAbstracto p1, ParticipanteAbstracto p2) {
		//como la consigna no aclara más, se implementa la batalla con dos participantes cualesquiera
		int i = this.formaDeGanar.compare(p1,p2);
		if (i>0)
			return p1;
		if (i<0)
			return p2; 
		return null; //empate 
	}
	
}
