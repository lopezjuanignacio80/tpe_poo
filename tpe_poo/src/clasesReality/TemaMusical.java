package clasesReality;
import java.util.ArrayList;
import java.util.Objects;

import filtros.Filtro;

public class TemaMusical {
	private String titulo;
	private String idioma;
	private ArrayList<String> generosMusicales;
	private ArrayList<String> instrumentos;
	private Filtro criterio;
	
	public TemaMusical(String titulo, String idioma, Filtro criterio) {
		super();
		this.titulo = titulo;
		this.idioma = idioma;
		this.generosMusicales= new ArrayList<>();
		this.instrumentos= new ArrayList<>();
		this.criterio=criterio; //el criterio por defecto que propone la consigna, se puede construir
		//con los filtros disponibles (lo mismo sucede con el tema final, que también es una instancia de 
		//esta clase
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public ArrayList<String> getGenerosMusicales() {
		return new ArrayList<>(this.generosMusicales);
	}
	
	public ArrayList<String> getInstrumentos() {
		return new ArrayList<>(this.instrumentos);
	}
	
	public void setCriterio(Filtro criterio) {
		this.criterio = criterio;
	}

	public boolean puedeSerInterpretadoPor(ParticipanteAbstracto p) {
		if (criterio!=null) {	
			return criterio.cumple(p);
		}
		return true;
	}
	
	public void addInstrumento(String instrumento) {
		if(!this.instrumentos.contains(instrumento))
			instrumentos.add(instrumento);
	}
	
	public void addGenero(String genero) {
		if(!this.generosMusicales.contains(genero))
			generosMusicales.add(genero);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemaMusical other = (TemaMusical) obj;
		return Objects.equals(this.getTitulo(), other.getTitulo());
	}
	
	
}
