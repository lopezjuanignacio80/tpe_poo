package filtros;

import clasesReality.ParticipanteAbstracto;


public abstract class Filtro { 
	public abstract boolean cumple(ParticipanteAbstracto participante);
}
