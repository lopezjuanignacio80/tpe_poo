package filtros;

import clasesReality.ParticipanteAbstracto;

public class FiltroAND extends Filtro {
	private Filtro criterio;
	private Filtro criterio2;

	public FiltroAND(Filtro criterio, Filtro criterio2) {
		this.criterio = criterio;
		this.criterio2 = criterio2;
	}

	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return criterio.cumple(participante) && criterio2.cumple(participante);
	}
}
