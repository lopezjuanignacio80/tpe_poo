package filtros;
import clasesReality.ParticipanteAbstracto;

public class FiltroCantParticipantesTocanInstrumento extends FiltroInstrumento {
	private int cantidadParticipantes;
	
	
	public FiltroCantParticipantesTocanInstrumento(String instrumento, int cant) {
		super(instrumento);
		this.cantidadParticipantes = cant;
	}


	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		if(participante.getPersonasTocanInstrumento(instrumentoBuscado)>=cantidadParticipantes)
			return true;	
		return false;
	}
}
