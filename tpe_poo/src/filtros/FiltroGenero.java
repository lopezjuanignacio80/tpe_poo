package filtros;

import clasesReality.ParticipanteAbstracto;

public class FiltroGenero extends Filtro {
	
	private String generoBuscado;
	
	public FiltroGenero(String genero) {
		this.generoBuscado= genero;
	}

	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return participante.prefiereGenero(generoBuscado);
	}	
}
