package filtros;

import clasesReality.ParticipanteAbstracto;

public class FiltroInstrumento extends Filtro {
	protected String instrumentoBuscado;
	
	public FiltroInstrumento(String instrumento) {
		this.instrumentoBuscado= instrumento;
	}
	
	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return participante.tocaInstrumento(instrumentoBuscado);
	}	

}
