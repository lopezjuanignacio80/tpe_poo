package filtros;

import clasesReality.ParticipanteAbstracto;

public class FiltroEdad extends Filtro {

	private int edadBuscada;

	public FiltroEdad(int edad) {
		this.edadBuscada= edad;
	}
	
	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return participante.getEdad()>edadBuscada;
	}	
}
