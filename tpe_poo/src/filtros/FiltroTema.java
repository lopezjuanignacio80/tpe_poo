package filtros;

import clasesReality.ParticipanteAbstracto;
import clasesReality.TemaMusical;

public class FiltroTema extends Filtro {
	private TemaMusical temaBuscado;
	
	
	public FiltroTema(TemaMusical temaBuscado) {
		super();
		this.temaBuscado =temaBuscado;
	}

	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return temaBuscado.puedeSerInterpretadoPor(participante);
		
	}
	
}
