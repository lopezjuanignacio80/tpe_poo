package filtros;

import clasesReality.ParticipanteAbstracto;

public class FiltroIdioma extends Filtro {

	private String idiomaBuscado;

	
	public FiltroIdioma(String idioma) {
		this.idiomaBuscado= idioma;
	}

	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return participante.cantaIdioma(idiomaBuscado);
	}	

}
