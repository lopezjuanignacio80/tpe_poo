package filtros;

import clasesReality.ParticipanteAbstracto;

public class FiltroNOT extends Filtro {
	private Filtro criterio;
	
	public FiltroNOT(Filtro criterio) {
		this.criterio = criterio;
	}

	@Override
	public boolean cumple(ParticipanteAbstracto participante) {
		return !criterio.cumple(participante);
	}

	
	
}
